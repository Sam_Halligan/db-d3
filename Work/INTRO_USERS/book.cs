﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace INTRO_USERS
{
    public partial class book : Form
    {
        public string name = "";
        public string name2 = "";
        /// <summary>
        /// //This BrowsePage method executes upon loading of the form
        /// </summary>
        public book(string CurrentUser)
        {
            InitializeComponent();

            label9.Text = CurrentUser;
            name2 = CurrentUser;
            using (SqlConnection sqlConnection = new SqlConnection(@"Data Source = localhost\; Database = Driving_Comp; Integrated Security = True"))
            {
                SqlCommand sqlCmd = new SqlCommand("SELECT FirstName, LastName FROM Instructors", sqlConnection);
                sqlConnection.Open();
                SqlDataReader sqlReader = sqlCmd.ExecuteReader();

                // name = SQL.read[2].ToString() + SQL.read[3].ToString();

                while (sqlReader.Read())
                {
                    comboBox1.Items.Add(sqlReader["FirstName"].ToString() + " " + (sqlReader["LastName"].ToString()));
                }
                sqlReader.Close();
            }
        }


        private void buttonLogin_Click(object sender, EventArgs e)
        {
            //You should have seen this from the register page, same code to switch forms.
            Hide();
            LoginPage login = new LoginPage();
            login.ShowDialog();
            Close();
        }

        /// <summary>
        /// Gets the social media id based on the social media name
        /// </summary>
        /// <param name="socialMedia">The name of the social media from combo box</param>
        /// <returns>The ID of the social media from database, blank string returned if not in database</returns>


        private void BrowsePage_Load(object sender, EventArgs e)
        {
            ResetColor();
        }

        public static string timeslot;
        public static string day;
        public static string instructor;
        public static string client;



        public void dateTimePicker1_ValueChanged(object sender, EventArgs e)
        {
            ResetColor();

            string Date = "", selectDay = "";
            Date = dateTimePicker1.Text.ToString();
            selectDay = Date;

            if (selectDay[0] == 'S' && selectDay[1] == 'u')
            {
                comboBox2.Visible = false;
                labelSunday.Visible = true;
            }
            else
            {
                comboBox2.Visible = true;
                labelSunday.Visible = false;
            }

        }
        public void ResetColor()
        {

            button9.BackColor = Color.FromKnownColor(KnownColor.Control);
            button13.BackColor = Color.FromKnownColor(KnownColor.Control);

        }

        /*   public void CheckInput()
           {
               string TimeI = "", DateI = "", InstructorNameI = "", CurrentUserI = "";

               TimeI = timeslot;
               DateI= dateTimePicker1.Text.ToString();
               InstructorNameI = comboBox1.Text;
               CurrentUserI = label9.Text;


               using (SqlConnection sqlConnection = new SqlConnection(@"Data Source = localhost\; Database = Driving_Comp; Integrated Security = True"))
               {
                   SqlCommand sqlCmd = new SqlCommand("SELECT * FROM Timeslot WHERE Time = @TimeI AND Date = @DateI AND InstructorName = @InstructorNameI AND Client = @CurrentUser", sqlConnection);
                   sqlConnection.Open();
                   SqlDataReader sqlReader = sqlCmd.ExecuteReader();

                   // name = SQL.read[2].ToString() + SQL.read[3].ToString();  (Time, Date, InstructorName, Client)

                   while (sqlReader.Read())
                   {
                      if (TimeI == "" && DateI == "" && InstructorNameI == "" && CurrentUserI == "")
                       {
                          // SQL.executeQuery("INSERT INTO Timeslot (Time, Date, InstructorName, Client) VALUES ('" + TimeI + "', '" + DateI + "','" + InstructorNameI + "', '" + CurrentUserI + "')");
                           MessageBox.Show("it Works!");

                       }
                       else
                       {
                           MessageBox.Show("Time unavalible. Please Pick Another");
                       }
                   }

                   sqlReader.Close();
               }
           } */

        public void button13_Click(object sender, EventArgs e) //submit button
        {
            timeslot = comboBox2.Text.ToString();
            //variables to be used
            string Time = "", Date = "", InstructorName = "", CurrentUser = "";

            Time = timeslot;
            Date = dateTimePicker1.Text.ToString();
            InstructorName = comboBox1.Text;
            CurrentUser = label9.Text;

            string TimeI = "", DateI = "", InstructorNameI = "", CurrentUserI = ""; // TEST CODE

            TimeI = Time;
            DateI = Date;
            InstructorNameI = InstructorName;
            CurrentUserI = CurrentUser;

            if (comboBox1.SelectedItem == null || comboBox2.SelectedItem == null)
            {
                MessageBox.Show("Error, Missing instructor or time/date.");

                /*  Hide();
                  BrowsePage browsePage = new BrowsePage();
                  browsePage.ShowDialog();
                  Close(); */

            }
            else
            { // TEST CODE STARTS ------------------------------------------------------------------------

                using (SqlConnection sqlConnection = new SqlConnection(@"Data Source = localhost\; Database = Driving_Comp; Integrated Security = True"))
                {
                    // System.Diagnostics.Debug.WriteLine($"########    SELECT * FROM Timeslot WHERE Time = '{TimeI}' AND Date = '{DateI}' AND InstructorName = '{InstructorNameI}' AND Client = '{CurrentUserI}'");


                    SqlCommand sqlCmd = new SqlCommand($"SELECT * FROM Timeslot WHERE Time = '{TimeI}' AND Date = '{DateI}' AND Instructorname = '{InstructorNameI}' AND Client = '{CurrentUserI}'", sqlConnection);
                    sqlConnection.Open();
                    SqlDataReader sqlReader = sqlCmd.ExecuteReader();

                    // name = SQL.read[2].ToString() + SQL.read[3].ToString();  (Time, Date, InstructorName, Client)

                    while (sqlReader.Read())
                    {
                        if (sqlReader.HasRows)
                        {
                            TimeI = sqlReader.GetString(1);
                            DateI = sqlReader.GetString(2);
                            InstructorNameI = sqlReader.GetString(3);
                            CurrentUserI = sqlReader.GetString(4);


                            if (TimeI == "" && DateI == "" && InstructorNameI == "" && CurrentUserI == "")
                            {

                                SQL.executeQuery("INSERT INTO Timeslot (Time, Date, Instructorname, Client) VALUES ('" + Time + "', '" + Date + "','" + InstructorName + "', '" + CurrentUser + "')");
                                //success message for the user to know it worked
                                MessageBox.Show("Successful - Your appointment is: " + Date + " at " + Time + " With " + InstructorName);


                            }
                            else
                            {
                                MessageBox.Show("Time unavalible. Please Pick Another");
                                return;
                            }
                        }




                    }

                    sqlReader.Close();
                }
                // CheckInput(); TEST CODE ENDS ---------------------------------------------------------------
                try
                {
                    SQL.executeQuery("INSERT INTO Timeslot (Time, Date, Instructorname, Client) VALUES ('" + Time + "', '" + Date + "','" + InstructorName + "', '" + CurrentUser + "')");
                    //success message for the user to know it worked
                    MessageBox.Show("Successful - Your appointment is: " + Date + " at " + Time + " With " + InstructorName);
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Time unavalible.");
                    return;
                }



                //Go back to the login page since we registered successfully to let the user log in
                /*  Hide();                                 //hides the register form
                 LoginPage login = new LoginPage();      //creates the login page as an object
                 login.ShowDialog();                     //shows the new login page form
                 this.Close();                           //closes the register form that was hidden */
            }
        }

        private void button9_Click(object sender, EventArgs e) // BLOCK OUT FULL DAY
        {
            this.Hide();
            //Create a Register Page object to change to
            LoginPage register = new LoginPage();
            //show the register page
            register.ShowDialog();
            //close the login page we are currently on
            this.Close();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            clienttime popup = new clienttime(name2);
            popup.ShowDialog();
        }
    }
}